import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
require('dotenv').config()

const config: PostgresConnectionOptions = {
  type: 'postgres',
  database: process.env.DATABASE_NAME,
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DATABASE_USERNAME,
  password: String(process.env.DATABASE_PASSWORD),
  entities: [__dirname + '/**/*.entity{.ts,.js}'],
  synchronize: true,
  // migrations: ['src/database/migrations/*{.ts,.js}'],
};

export default config;