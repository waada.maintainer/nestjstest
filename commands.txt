nest g module auth
nest g service auth
nest g module users
nest g service users


// Docker
docker build -t zenith-backend .
docker run -p 3000:3000 zenith-backend
docker run -p 3000:3000 --network="host" zenith-backend


npm install @nestjs/typeorm typeorm pg graphql @nestjs/graphql