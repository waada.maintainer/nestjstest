import { Module } from '@nestjs/common';
import { ProfileService } from './profile.service';
import { Profile } from 'src/entity/profile.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Profile])],
  providers: [ProfileService]
})
export class ProfileModule {}
