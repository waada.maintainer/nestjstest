import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService) { }

    /**
     * AuthGuard -> is used to as a middleware 
     * @param req 
     * @returns 
     */
    @UseGuards(AuthGuard('local'))
    @Post('login')
    async login(@Body() req:any) {
        return await this.authService.authenticate(req);
    }

    @Get('protect')
    @UseGuards(AuthGuard('jwt'))
    getUser(){
        return "Protected routed"
    }
}
