import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from './local.strategy';
import { UserService } from 'src/user/user.service';
import { UserModule } from 'src/user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { User } from 'src/entity/user.entity';
import { JwtStrategy } from './jwt.strategy';
import { ProfileModule } from 'src/profile/profile.module';
import { Profile } from 'src/entity/profile.entity';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([User,Profile]),
    JwtModule.register({
      global: true,
      secret: "455445454545",
      signOptions: { expiresIn: '60s' },
    }),
  ],
  providers: [AuthService , LocalStrategy , UserService , JwtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
