import { Injectable, Res } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { log } from 'console';
import { UserNotFoundException } from 'exceptions/user-not-found.exception';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';
import { async } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(private userService: UserService, private jwtService: JwtService) { }

  /**
   * 
   * @param username 
   * @param password 
   * @returns 
   */
  async validateUser(username: string, password: string) {
    const userObj = await this.userService.findUserByEmail(username);
    if (userObj && (await bcrypt.compare(password, userObj.password))) {
      const { password, ...result } = userObj;
      return result;
    }
    return null;
  }

  /**
   * 1) Find user by email
   * 2) compare user's pass and db is correct or not 
   * @param user 
   * @returns User object
   */
  async authenticate(user: any) {
    const userObj = await this.validateUser(user.email, user.password);
    console.log(userObj);

    const payload = { sub: userObj.id, username: userObj.name };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }


}
