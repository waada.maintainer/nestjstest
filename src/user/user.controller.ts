import { Body, Controller, Get, Param, Post, Res } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/user.dto';

@Controller('user')
export class UserController {

    constructor(private readonly userService: UserService) { }

    @Get('all')
    async findALL(@Res() res: any) {
        console.log("Hit-1");
        return await this.userService.findAll().then((result) => {
            console.log("Hit-2");
            console.log(result);
            return res.json({
                result
            });
        }).catch((err) => {
            return err;
        });
    }

    @Post('create')
    createUser(@Body() req: any) {
        return req;
    }

    @Get('find-one/:id')
    async findOne(@Param('id') id: number) {
        return await this.userService.findUserById(id).then((result) => {
            return result
        }).catch((err) => {
            return err;
        });
    }

    @Post('save')
    async saveUser(@Body() userReq: CreateUserDto) {
        console.log(userReq);
        if (userReq) {
            console.log("LOG");
        }

        const profileRes = {
            "address": userReq.address,
            "role": userReq.role,
        }

        await this.userService.addUser(userReq,profileRes);
    }
}

