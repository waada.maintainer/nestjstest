import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entity/user.entity';
import { ProfileService } from 'src/profile/profile.service';
import { Profile } from 'src/entity/profile.entity';
import { ProfileModule } from 'src/profile/profile.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User,Profile]),
  ],
  providers: [UserService],
  controllers: [UserController]
})
export class UserModule {}
