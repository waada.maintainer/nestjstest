import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Profile } from 'src/entity/profile.entity';
import { User } from 'src/entity/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepo: Repository<User>,
        @InjectRepository(Profile)
        private profileRepository: Repository<Profile>
    ) { }

    findAll() {
        return this.userRepo.find();
    }

    findUserById(id: number) {
        let user = this.userRepo.find({
            where: {
                id: id
            }
        })
        return user;
    }

    findUserByEmail(email: string) {
        let user = this.userRepo.findOne({
            where: {
                email: email
            }
        })
        return user;
    }

    async addUser(userReq: any, profileData: any) {
        const createUserObj = this.userRepo.create(userReq);
        const savedUser = await this.userRepo.save(createUserObj);
        const userId = savedUser['id'];
        console.log(`User ID: ${userId}`);

        // * When user is saved then save user's profile data in profile DB.
        if (savedUser) {
            const profileCreateObj = this.profileRepository.create({
                ...profileData,
                user: savedUser, // Associate the profile with the user
            });
            return this.profileRepository.save(profileCreateObj);
        }

    }
}
