import { IsNotEmpty, IsString } from "class-validator";

export class CreateUserDto{

    @IsString()
    @IsNotEmpty({ message: 'Username should be a string' })
    name:string; 

    @IsString()
    @IsNotEmpty()
    email : string;

    @IsNotEmpty()
    password:string;

    address:string;

    role:string;


}