import { BeforeInsert, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import * as bcrypt from 'bcrypt';
import { Info } from "./Info.entity";
import { Profile } from "./profile.entity";

@Entity()
export class User{

@PrimaryGeneratedColumn('increment')
id:number 

@Column()
name:string

@Column()
email:string

@Column()
password:string

@OneToOne(()=> Info , {eager : false})
@JoinColumn()
info : Info

// @OneToOne(()=> Profile , (profile) => profile.user)
// @JoinColumn()
// profile : Profile


@BeforeInsert()
async hashPassword(){
    this.password = await bcrypt.hash(this.password, 10)
}

}

