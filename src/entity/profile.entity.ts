import { BeforeInsert, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity()
export class Profile{

@PrimaryGeneratedColumn('increment')
id:number 

@Column()
address:string

@Column()
role:string

@OneToOne(() => User)
@JoinColumn()
user: User

}