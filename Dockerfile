# Use the official Node.js image as the base image
FROM node:20

# Set the working directory in the container
WORKDIR '/app'

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of your application code to the container
COPY . .

# Expose the port your NestJS app is running on (e.g., 3000)
EXPOSE 3000

# Start your NestJS application
CMD ["npm", "run", "start:dev"]
